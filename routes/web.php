<?php

use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->middleware('auth')->group(function(){
    Route::resource('/', 'DashboardController');
    Route::resource('/voucher', 'VoucherController');
    Route::resource('/heading', 'HeadingController');
    Route::resource('/order', 'OrderController');
    Route::resource('/partner', 'PartnerController');
    Route::resource('/product', 'ProductController');
    Route::resource('/mall', 'MallController');
    Route::resource('/campaign', 'CampaignController');
    Route::resource('/settlement', 'SettlementController');
    Route::resource('/relationship', 'RelationshipController');
    Route::resource('/corporate', 'CorporateController');
    Route::resource('/agency', 'AgencyController');
    Route::resource('/directory', 'DirectoryController');
    Route::resource('/banner', 'BannerController');
    Route::resource('/user', 'UserController');
    Route::resource('/customer', 'CustomerController');
});
