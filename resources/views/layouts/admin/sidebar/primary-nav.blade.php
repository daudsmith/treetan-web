<nav id="js-primary-nav" class="primary-nav" role="navigation">
    <div class="nav-filter">
        <div class="position-relative">
            <input type="text" id="nav_filter_input" placeholder="Filter menu" class="form-control" tabindex="0">
            <a href="#" onclick="return false;" class="btn-primary btn-search-close js-waves-off" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar">
                <i class="fal fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="info-card">
        <img src="{{asset('themes/smartadmin/4.5.0/img/demo/avatars/avatar-admin.png')}}" class="profile-image rounded-circle" alt="Seweit Hotroyman">
        <div class="info-card-text">
            <a href="#" class="d-flex align-items-center text-white">
                <span class="text-truncate text-truncate-sm d-inline-block">
                    Seweit Hotroyman
                </span>
            </a>
            <span class="d-inline-block text-truncate text-truncate-sm">Jakarta, Indonesia</span>
        </div>
        <img src="{{asset('themes/smartadmin/4.5.0/img/card-backgrounds/cover-2-lg.png')}}" class="cover" alt="cover">
        <a href="#" onclick="return false;" class="pull-trigger-btn" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar" data-focus="nav_filter_input">
            <i class="fal fa-angle-down"></i>
        </a>
    </div>
    <ul id="js-nav-menu" class="nav-menu">
        <li>
            <a href="#" title="Dashboard" data-filter-tags="dashboard">
                <i class="fal fa-home"></i>
                <span class="nav-link-text" data-i18n="nav.dashboard">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="#" title="Admin Management" data-filter-tags="admin management">
                <i class="fal fa-user-lock"></i>
                <span class="nav-link-text" data-i18n="nav.admin_management">Admin Management</span>
            </a>
        </li>
        <li>
            <a href="#" title="Voucher Management" data-filter-tags="voucher management">
                <i class="fal fa-ticket-alt"></i>
                <span class="nav-link-text" data-i18n="nav.voucher_management">Voucher Management</span>
            </a>
        </li>
        <li>
            <a href="#" title="Heading Management" data-filter-tags="heading management">
                <i class="fal fa-columns"></i>
                <span class="nav-link-text" data-i18n="nav.heading_management">Heading Management</span>
            </a>
        </li>
        <li>
            <a href="#" title="Order Management" data-filter-tags="order management">
                <i class="fal fa-shopping-cart"></i>
                <span class="nav-link-text" data-i18n="nav.order_management">Order Management</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="All Order Detail" data-filter-tags="order all order detail">
                        <span class="nav-link-text" data-i18n="nav.order_management_all_order_detail">All Order Detail</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Bank Account Order" data-filter-tags="order bank account order">
                        <span class="nav-link-text" data-i18n="nav.order_management_bank_account_order">Bank Account Order</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Tretan Distribution Channel" data-filter-tags="order tretan distribution channel">
                        <span class="nav-link-text" data-i18n="nav.order_management_tretan_distribution_channel">Tretan Distribution Channel</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Walk Ins" data-filter-tags="order walk ins">
                        <span class="nav-link-text" data-i18n="nav.order_management_walk_ins">Walk Ins</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Referrals" data-filter-tags="order referrals">
                        <span class="nav-link-text" data-i18n="nav.order_management_referrals">Referrals</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Order Confirmation Review" data-filter-tags="order order confirmation review">
                        <span class="nav-link-text" data-i18n="nav.order_management_order_confirmation_review">Order Confirmation Review</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Upload & Request Document" data-filter-tags="order upload & request document">
                        <span class="nav-link-text" data-i18n="nav.order_management_upload_request_document">Upload & Request Document</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Partner Management" data-filter-tags="partner management">
                <i class="fal fa-handshake"></i>
                <span class="nav-link-text" data-i18n="nav.partner_management">Partner Management</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Bank Partner" data-filter-tags="partner bank partner">
                        <span class="nav-link-text" data-i18n="nav.bank_partner">Bank Partner</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Corporate Partners" data-filter-tags="partner corporate partners">
                        <span class="nav-link-text" data-i18n="nav.corporate_partners">Corporate Partners</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Distributor/Agency Partners" data-filter-tags="partner distributor agency partners">
                        <span class="nav-link-text" data-i18n="nav.distributor_agency_partners">Distributor/Agency Partners</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Umroh Travel Partner" data-filter-tags="partner umroh travel partner">
                        <span class="nav-link-text" data-i18n="nav.umroh_travel_partner">Umroh Travel Partner</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Database Product" data-filter-tags="database product">
                <i class="fal fa-luggage-cart"></i>
                <span class="nav-link-text" data-i18n="nav.database_product">Database Product</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Produk Umroh" data-filter-tags="database produk umroh">
                        <span class="nav-link-text" data-i18n="nav.produk_umroh">Produk Umroh</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Produk Rekening Bank" data-filter-tags="database produk rekening bank">
                        <span class="nav-link-text" data-i18n="nav.produk_rekening_bank">Produk Rekening Bank</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Tretan Mall" data-filter-tags="tretan mall">
                <i class="fal fa-store"></i>
                <span class="nav-link-text" data-i18n="nav.tretan_mall">Tretan Mall</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Campaign Management" data-filter-tags="campaign management">
                        <span class="nav-link-text" data-i18n="nav.campaign_management">Campaign Management</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Admin Management" data-filter-tags="admin management">
                        <span class="nav-link-text" data-i18n="nav.admin_management">Admin Management</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Customer Management" data-filter-tags="customer management">
                        <span class="nav-link-text" data-i18n="nav.customer_management">Customer Management</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Settlement Management" data-filter-tags="settlement management">
                        <span class="nav-link-text" data-i18n="nav.settlement_management">Settlement Management</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Heading Management" data-filter-tags="heading management">
                        <span class="nav-link-text" data-i18n="nav.heading_management">Heading Management</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Banner Management" data-filter-tags="banner management">
                        <span class="nav-link-text" data-i18n="nav.banner_management">Banner Management</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Product Database" data-filter-tags="product database">
                        <span class="nav-link-text" data-i18n="nav.product_database">Product Database</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Voucher Management" data-filter-tags="voucher management">
                        <span class="nav-link-text" data-i18n="nav.voucher_management">Voucher Management</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="CR Management" data-filter-tags="cr management">
                        <span class="nav-link-text" data-i18n="nav.cr_management">CR Management</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Campaign Management" data-filter-tags="campaign management">
                <i class="fal fa-ad"></i>
                <span class="nav-link-text" data-i18n="nav.campaign_management">Campaign Management</span>
            </a>
        </li>
        <li>
            <a href="#" title="Settlement Management" data-filter-tags="settlement management">
                <i class="fal fa-money-check-alt"></i>
                <span class="nav-link-text" data-i18n="nav.settlement_management">Settlement Management</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Settlement" data-filter-tags="settlement">
                        <span class="nav-link-text" data-i18n="nav.settlement">Settlement</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Settlement Agency Disburse" data-filter-tags="settlement agency disburse">
                        <span class="nav-link-text" data-i18n="nav.settlement_agency_disburse">Settlement Agency Disburse</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="CR Management" data-filter-tags="cr management">
                <i class="fal fa-bullhorn"></i>
                <span class="nav-link-text" data-i18n="nav.cr_management">CR Management</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Broadcast" data-filter-tags="broadcast">
                        <span class="nav-link-text" data-i18n="nav.broadcast">Broadcast</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Inbox" data-filter-tags="inbox">
                        <span class="nav-link-text" data-i18n="nav.inbox">Inbox</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Corporate Management" data-filter-tags="corporate management">
                <i class="fal fa-building"></i>
                <span class="nav-link-text" data-i18n="nav.corporate_management">Corporate Management</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Corporate Vouchers" data-filter-tags="corporate vouchers">
                        <span class="nav-link-text" data-i18n="nav.corporate_vouchers">Corporate Vouchers</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Subsidies" data-filter-tags="subsidies">
                        <span class="nav-link-text" data-i18n="nav.subsidies">Subsidies</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Employees" data-filter-tags="employees">
                        <span class="nav-link-text" data-i18n="nav.employees">Employees</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Agency Distributor" data-filter-tags="agency distributor">
                <i class="fal fa-chart-network"></i>
                <span class="nav-link-text" data-i18n="nav.agency_distributor">Agency Distributor</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Team Database" data-filter-tags="team database">
                        <span class="nav-link-text" data-i18n="nav.team_database">Team Database</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Directory Management" data-filter-tags="directory management">
                <i class="fal fa-map-marked-alt"></i>
                <span class="nav-link-text" data-i18n="nav.directory_management">Directory Management</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Airports" data-filter-tags="airports">
                        <span class="nav-link-text" data-i18n="nav.airports">Airports</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Accommodation" data-filter-tags="accommodation">
                        <span class="nav-link-text" data-i18n="nav.accommodation">Accommodation</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="Airlines" data-filter-tags="airlines">
                        <span class="nav-link-text" data-i18n="nav.airlines">Airlines</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="City" data-filter-tags="city">
                        <span class="nav-link-text" data-i18n="nav.city">City</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Banner Management" data-filter-tags="banner management">
                <i class="fal fa-blog"></i>
                <span class="nav-link-text" data-i18n="nav.banner_management">Banner Management</span>
            </a>
        </li>
        <li>
            <a href="#" title="User Access" data-filter-tags="user access">
                <i class="fal fa-users-cog"></i>
                <span class="nav-link-text" data-i18n="nav.user_access">User Access</span>
            </a>
            <ul>
                <li>
                    <a href="#" title="Role Access" data-filter-tags="role access">
                        <span class="nav-link-text" data-i18n="nav.role_access">Role Access</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="Customer Database" data-filter-tags="customer database">
                <i class="fal fa-users-class"></i>
                <span class="nav-link-text" data-i18n="nav.customer_database">Customer Database</span>
            </a>
        </li>
    </ul>
    <div class="filter-message js-filter-message bg-success-600"></div>
</nav>