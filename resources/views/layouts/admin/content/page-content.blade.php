<main id="js-page-content" role="main" class="page-content">
    @yield('content')
</main>