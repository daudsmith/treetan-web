@extends('layouts.base')

@section('content')
    <div class="page-wrapper auth">
        <div class="page-inner bg-brand-gradient">
            <div class="page-content-wrapper bg-transparent m-0">
                <div class="height-10 w-100 shadow-lg px-4 bg-brand-gradient">
                    <div class="d-flex align-items-center container p-0">
                        <div class="page-logo width-mobile-auto m-0 align-items-center justify-content-center p-0 bg-transparent bg-img-none shadow-0 height-9 border-0">
                            <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
                                <img src="{{asset('themes/smartadmin/4.5.0/img/logo.png')}}" alt="Tretan WebApp" aria-roledescription="logo">
                                <span class="page-logo-text mr-1">{{ config('app.name', 'Laravel') }} WebApp</span>
                            </a>
                        </div>
                        <a href="{{ route('register') }}" class="btn-link text-white ml-auto">
                            Create Account
                        </a>
                    </div>
                </div>
                <div class="flex-1" style="background: url(themes/smartadmin/4.5.0/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;">
                    <div class="container py-4 py-lg-5 my-lg-5 px-4 px-sm-0">
                        <div class="row">
                            <div class="col col-md-6 col-lg-7 hidden-sm-down">
                                <h2 class="fs-xxl fw-500 mt-4 text-white">
                                    Quote of the day
                                    <small class="h3 fw-300 mt-3 mb-5 text-white opacity-60">
                                        <blockquote class="blockquote text-left">
                                            <p class="mb-0">"Give them quality. That is the best kind of advertising."</p>
                                            <footer class="blockquote-footer"><cite title="Source Title">Milton Hershey, entrepreneur</cite></footer>
                                        </blockquote>
                                    </small>
                                    <small class="h3 fw-300 mt-3 mb-5 text-white opacity-60">
                                        <blockquote class="blockquote text-left">
                                            <p class="mb-0">"People don’t buy what you do, they buy why you do it."</p>
                                            <footer class="blockquote-footer"><cite title="Source Title">Simon Sinek, author and marketing consultant</cite></footer>
                                        </blockquote>
                                    </small>
                                    <small class="h3 fw-300 mt-3 mb-5 text-white opacity-60">
                                        <blockquote class="blockquote text-left">
                                            <p class="mb-0">"Before you create any more ‘great content,’ figure out how you are going to market it first."</p>
                                            <footer class="blockquote-footer"><cite title="Source Title">Joe Pulizzi, content marketer and strategist</cite></footer>
                                        </blockquote>
                                    </small>
                                </h2>
                                <div class="d-sm-flex flex-column align-items-center justify-content-center d-md-block">
                                    <div class="px-0 py-1 mt-5 text-white fs-nano opacity-50">
                                        Find us on social media
                                    </div>
                                    <div class="d-flex flex-row opacity-70">
                                        <a href="#" class="mr-2 fs-xxl text-white">
                                            <i class="fab fa-facebook-square"></i>
                                        </a>
                                        <a href="#" class="mr-2 fs-xxl text-white">
                                            <i class="fab fa-twitter-square"></i>
                                        </a>
                                        <a href="#" class="mr-2 fs-xxl text-white">
                                            <i class="fab fa-instagram-square"></i>
                                        </a>
                                        <a href="#" class="mr-2 fs-xxl text-white">
                                            <i class="fab fa-youtube-square"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4 ml-auto">
                                <h1 class="text-white fw-300 mb-3 d-sm-block d-md-none">
                                    Secure login
                                </h1>
                                <div class="card p-4 rounded-plus bg-faded">
                                    <form id="js-login" novalidate="" action="{{ route('login') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label class="form-label" for="username">{{ __('E-Mail Address') }}</label>
                                            <input type="email" id="username" class="form-control form-control-lg @error('email') is-invalid @enderror" placeholder="Enter your e-mail" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                            @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                            <div class="help-block">Your login email to app</div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="password">{{ __('Password') }}</label>
                                            <input type="password" id="password" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="Enter your password" name="password" required autocomplete="current-password">
                                            @error('password')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                            <div class="help-block">Your password</div>
                                        </div>
                                        <div class="form-group text-left">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="rememberme">
                                                <label class="custom-control-label" for="rememberme"> Remember me</label>
                                            </div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-lg-6 pl-lg-1 my-2">
                                                <button id="js-login-btn" type="submit" class="btn btn-info btn-block btn-lg">{{ __('Login') }}</button>
                                            </div>
                                            <div class="col-lg-6 pl-lg-1 my-2">
                                                @if (Route::has('password.request'))
                                                    <a class="btn btn-danger btn-block btn-lg" href="{{ route('password.request') }}">
                                                        {{ __('Forgot') }}
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="position-absolute pos-bottom pos-left pos-right p-3 text-center text-white">
                            2020 © Tretan by&nbsp;<a href='https://www.seweithotroyman.com/' class='text-white opacity-40 fw-500' title='seweithotroyman.com' target='_blank'>seweithotroyman.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
